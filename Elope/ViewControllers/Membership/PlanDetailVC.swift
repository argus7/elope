//
//  PlanDetailVC.swift
//  Elope
//
//  Created by veer on 16/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class PlanDetailVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        viewInitilization()
        // Do any additional setup after loading the view.
    }
    
    private func viewInitilization () {
        self.setupRightSwipeGesture(.right)
//           UIApplication.shared.statusBarView?.backgroundColor = UIColor.appColor
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func onClickNextbutton(_ sender: UIButton) {
        
        print("++++++++++++ ---------- >>>>>  next button clicked")
        let vc  = storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
