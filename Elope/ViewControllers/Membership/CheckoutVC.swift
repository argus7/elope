//
//  CheckoutVC.swift
//  Elope
//
//  Created by veer on 16/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class CheckoutVC: UIViewController {

    @IBOutlet weak var accountDetailsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Buisness logic
        self.setupRightSwipeGesture(.right)
    }
   
    private func viewInitilization () {
        print("++++++++++++ ---------- >>>>> initilize all prerequirements of the view on load")
        self.setupRightSwipeGesture(.left)
        accountDetailsTable.register(UINib(nibName: "AccountDetailCell", bundle: Bundle.main), forCellReuseIdentifier: "AccountDetailCell")
        accountDetailsTable.delegate = self
        accountDetailsTable.dataSource = self

    }
    @IBAction func onClickBackButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>  Bck button is Pressed")
        self.navigationController?.popViewController(animated: true)
    }
}
extension CheckoutVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "AccountDetailCell", for: indexPath) as! AccountDetailCell
        return cell
    
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        <#code#>
//    }
}
