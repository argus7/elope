//
//  RegisterVC.swift
//  Elope
//
//  Created by Anand Yadav on 27/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import TLFloatLabelTextField
import Alamofire
class RegisterVC: UIViewController {
    
    @IBOutlet var singInButton: UIButton!
    @IBOutlet var emailtextfield:TLFloatLabelTextField!
    @IBOutlet var passwordtextfield:TLFloatLabelTextField!
    @IBOutlet var confirmpasswordtextfield:TLFloatLabelTextField!
    @IBOutlet var usernametextfield:TLFloatLabelTextField!
    
    
    var registrationResponseData : RegistrationResponse?
    
    //MARK:- VIEW LIFE CYCLE METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Do any additional setup after loading the view.
        singInButton.halfTextColorChange(fullText: "Already have an account? Sign In", changeText: "Sign In")
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    /// Validate Fields
    private func validate()  -> Bool{
        do {
            let username = try usernametextfield.validatedText(validationType: ValidatorType.username)
            let email = try emailtextfield.validatedText(validationType: ValidatorType.email)
            let password = try passwordtextfield.validatedText(validationType: ValidatorType.password)
            let data = RegisterData(email: email, password: password, username: username)
            print(data)
            
            return true
            //            let otpVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OTPVC") as? OTPVC
            //            self.navigationController?.pushViewController(otpVC!, animated: true)
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
            return false
        }
    }
    
    private func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- IBACTION METHOD
    @IBAction private func signInPressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func signupPressed(sender:UIButton){
        let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        if validate(){
        //            serviceRequestForRegistration()
        //        }
    }
    
    @IBAction private func backbuttonPressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}



//MARK:- Network Communication

extension RegisterVC {
    
    /***
     This method is used to Request Data for Network parameter
     
     # Usage #
     
     - To request  new user  Data to register in our application
     
     ````
     Username , password , Email Id
     `````
     # KEYS #
     "email": "temp@temp.com",
     "fname": "temp",
     "lname": "temp",
     "password": "temp"
     */
    
    private func getParamsForRegistration() -> [String : Any] {
        
        
        var paramDic = [String : Any]()
        
        paramDic["email"] = self.emailtextfield.text
        paramDic["fname"] = self.usernametextfield.text
        paramDic["lname"] = self.usernametextfield.text
        paramDic["password"] = self.passwordtextfield.text
        
        return paramDic
    }
    
    
    /***
     This method is used to Request our Network
     
     # Usage #
     
     - To request new user to register in our application
     
     # Network Requirements #
     
     - Base Url
     
     - Header
     
     - Parameter
     
     ````
     Username , password , Email Id
     
     */
    
    fileprivate func serviceRequestForRegistration() {
        
        Service.shared.serviceRrequest(URL: URLConstants.SignUp, params: getParamsForRegistration(), header: getheaderData()) { (response) in
            print(response)
            
            self.registrationResponseData = RegistrationResponse(fromJson: response)
            let responseCode = self.registrationResponseData?.status ?? 100
            switch responseCode {
            case 200 :
                print("Succesfully registerd")
                let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                self.navigationController?.pushViewController(vc, animated: true)
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.registrationResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
                break
            case 400 ,401,402,404 :
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.registrationResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            default:
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.registrationResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            }
            
        }
    }
    
}


