//
//  SubscriptionPlanVC.swift
//  Elope
//
//  Created by Anand Yadav on 30/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class SubscriptionPlanVC: UIViewController {
    
    @IBOutlet private weak var iCarouselView: iCarousel!
    
    @IBOutlet weak var pagerView: ElopeCustomPageControlView!
    
    
    var imgArr = [ UIImage(named:"subsplan"),
                    UIImage(named:"subsplan") ,
                    UIImage(named:"subsplan")
                    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Buisness Logic
        self.setupinitilization()
    }
    
    //MARK:- initilize view

    private func setupinitilization() {
        iCarouselView.type = .rotary
        iCarouselView.contentMode = .scaleAspectFit
        iCarouselView.isPagingEnabled = true
    
        
        //PageView Setup
        self.pagerView.isIndicatorChanged = IndicatorType.color
        self.pagerView.numberOfPages = imgArr.count
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
    
    
    //MARK:- ALL IBACTIONs METHODs
    
    @IBAction func skipbuttonPressed(sender:UIButton){

        //Set up center panel
        let searchflightvc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchFlightVC") as? SearchFlightVC
        let navigationVC: UINavigationController = UINavigationController(rootViewController: searchflightvc!)
//        let homenavigationController = UINavigationController.init(rootViewController: searchflightvc!)
//        homenavigationController.navigationBar.setBackgroundImage(UIImage(named: "top bgg"), for: UIBarMetrics.default)
//        homenavigationController.navigationBar.shadowImage = UIImage()
//        homenavigationController.navigationBar.isTranslucent = true
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        
        //Set up left menu panel
        let leftMenuVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LeftMenuVC") as? LeftMenuVC
        leftMenuVC?.searchFlightViewController = navigationVC
        
        let slideMenuController = SlideMenuController(mainViewController: navigationVC, leftMenuViewController: leftMenuVC!)
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    
    @IBAction func proceedbuttonPressed(sender:UIButton){
        
        print("++++++++++++ ---------- >>>>> Proceesing for plan details")

        let vc = ElopeStoryBoard.instantiateViewController(withIdentifier: "PlanDetailVC") as! PlanDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
}

extension SubscriptionPlanVC: iCarouselDelegate, iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return imgArr.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var imageView: UIImageView!
        
        if view == nil {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 40, height: 200))
            imageView.contentMode = .scaleAspectFit
            
        } else {
            imageView = view as? UIImageView
        }
        
        imageView.image = imgArr[index]
        self.pagerView.currentPage = index

        return imageView
    }
    
    
}
