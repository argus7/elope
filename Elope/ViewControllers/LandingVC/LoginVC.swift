//
//  LoginVC.swift
//  Elope
//
//  Created by Anand Yadav on 22/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import TLFloatLabelTextField
import Alamofire
class LoginVC: UIViewController {
    
    @IBOutlet weak var emailtextfield:TLFloatLabelTextField!
    @IBOutlet weak var passwordtextfield:TLFloatLabelTextField!
    @IBOutlet weak var registerNowButton: UIButton!
    var loginResponseData : LoginResponse?
    
    //MARK:- VIEW LIFE CYCLE METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        //Do any additional setup after loading the view.
        registerNowButton?.halfTextColorChange(fullText: "Don't have an account? Register now", changeText: "Register now")
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - INSTANCE METHOD
    private func validate() -> Bool {
        do {
            let email = try emailtextfield.validatedText(validationType: ValidatorType.email)
            let password = try passwordtextfield.validatedText(validationType: ValidatorType.password)
            let data = RegisterData(email: email, password: password, username: "")
            print(data)
            return true
        } catch(let error) {
            UIAlertController.showAlert(title: KeyConstants.APP_Name, message: (error as! ValidationError).message , viewcontroller: self,Transition: .Push)
            return false
        }
    }
    
    private func createSemiModalViewController() -> CustomAlertMessageViewController {
        
        let controller = CustomAlertMessageViewController()
        
        controller.maxWidth = 420
        controller.minHeight = 200
        
        controller.closeButton.setTitle("Done", for: .normal)
        
        let label = UILabel()
        label.text = "Registration Successfully"
        label.textAlignment = .center
        controller.addArrangedSubview(view: label)
        
        let imageView = UIImageView(image: UIImage (named: "Confirm ticket icon"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.lightGray
        controller.addArrangedSubview(view: imageView, height: 120)
        
        let secondLabel = UILabel()
        secondLabel.textAlignment = .center
        secondLabel.numberOfLines = 0
        secondLabel.text = "Your registration is now complete. Thank you for creating an account."
        controller.addArrangedSubview(view: secondLabel)
        
        controller.insertArrangedSubview(view: imageView, at:0)
        
        
        return controller
    }
    
    
    
    //MARK:- IBACTION METHOD
    @IBAction private func loginButtonPressed(sender:UIButton){
        
        if validate() {
            print("seuccess validate all textfields")
            serviceRequestForLogin()
        }
        else{
            print("some error occured")
        }
  
    }
    
    @IBAction private func remembermePressed(sender:UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction private func registerNowPressed(sender:UIButton){
        let registerVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
        self.navigationController?.pushViewController(registerVC!, animated: true)
    }
    
    @IBAction private func forgotpasswordPressed(sender:UIButton){
        let forgotPasswordVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword
        self.navigationController?.pushViewController(forgotPasswordVC!, animated: true)
    }
    
}
// MARK: - Network calling
extension LoginVC {
    
    /***
     This method is used to Request Data for Network parameter
     
     # Usage #
     
     - To request  new user  Data to Login in our application
     
     ````
    password , Email Id
     `````
     # KEYS #
     ```````
     "email": "temp@temp.com",
     "password": "temp"
     */
    
    private func getParamsForLogin() -> [String:Any] {
        
        var paramDic = [String : Any]()
        paramDic["email"] = self.emailtextfield.text
        paramDic["password"] = self.passwordtextfield.text
        
        return paramDic
    }
    
    
   
    
    /***
     This method is used to Request our Network

     # Usage #
     
     - To request existing user to Log in our application

     # Network Requirements #
     
     - Base Url
     
     - Header
     
     - Parameter
     ````
     password , Email Id
     `````
     # KEYS #
     `````
     "email": "temp@temp.com",
     "password": "temp"
     
     
     */
    func serviceRequestForLogin () {
        
        Service.shared.serviceRrequest(URL: URLConstants.Login, params: getParamsForLogin(), header: getheaderData()) { (response) in
            print(response)
           
            self.loginResponseData = LoginResponse(fromJson: response)
            
            let responseCode = self.loginResponseData?.status ?? 100
            switch responseCode {
            case 200 :
                print("Succesfully registerd")
                let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                self.navigationController?.pushViewController(vc, animated: true)
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.loginResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
                break
            case 400 ,401,402,404 :
                
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.loginResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)

            default:
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.loginResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)

            }
        }
    }
}

