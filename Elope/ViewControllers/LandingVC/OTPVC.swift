//
//  OTPVC.swift
//  Elope
//
//  Created by Anand Yadav on 28/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import SVPinView
import IHProgressHUD
class OTPVC: UIViewController {
    
    @IBOutlet var lblminutesleft: UILabel!
    @IBOutlet var pinView:SVPinView!
    var transparentView = UIView()
    var alertMessageView = UIView()
    
    var otpResponseData : OTPResponse?
    public var userEmail : String?
    let height: CGFloat = 250
    //MARK:- VIEW LIFE CYCLE METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Do any additional setup after loading the view.
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        
        self.configurePinView()
        
    }
    
    //MARK:- INSTANCE METHOD
    private func configurePinView() {
        
        pinView.pinLength = 4
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 20
        pinView.textColor = ColorConstants.appPrimaryColor
        pinView.borderLineColor = UIColor.lightGray
        pinView.activeBorderLineColor = ColorConstants.appPrimaryColor
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = true
        pinView.allowsWhitespaces = false
        pinView.style = .underline
        //        pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
        //        pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 15
        pinView.activeFieldCornerRadius = 15
        pinView.placeholder = "******"
        pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
        }
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    private func didFinishEnteringPin(pin:String) {
        //showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .default
    //    }
    
    //MARK:- IBACTION METHOD
    
    
    
    
    private func createCustomAlertViewController() -> CustomAlertMessageViewController {
        
        let controller = CustomAlertMessageViewController()
        
        controller.maxWidth = 420
        controller.minHeight = 200
        
        controller.closeButton.setTitle("Done", for: .normal)
        
        let label = UILabel()
        label.text = "Registration Successfully"
        label.textAlignment = .center
        controller.addArrangedSubview(view: label)
        
        let imageView = UIImageView(image: UIImage (named: "Confirm ticket icon"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.lightGray
        controller.addArrangedSubview(view: imageView, height: 120)
        
        let secondLabel = UILabel()
        secondLabel.textAlignment = .center
        secondLabel.numberOfLines = 0
        secondLabel.text = "Your registration is now complete. Thank you for creating an account."
        controller.addArrangedSubview(view: secondLabel)
        
        controller.insertArrangedSubview(view: imageView, at:0)
        
        
        return controller
    }
    
    
    @objc private func delayedAction(){
        IHProgressHUD.dismiss()
        
    }
    
    @objc private func onClickTransparentView() {
        let screenSize = UIScreen.main.bounds.size
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.alertMessageView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: self.height)
        }, completion: nil)
    }
    
    @IBAction private func cancelbuttonPressed(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction private func verifybuttonPressed(sender:UIButton){
        
        let chooseSubscriptionVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionPlanVC") as? SubscriptionPlanVC
        self.navigationController?.pushViewController(chooseSubscriptionVC!, animated: true)
        if validateFields(){
            print("case verified")
            
        }
        else {
            print("Error in validation")
        }
        
    }
    
    @IBAction private func resendcodebuttonPressed(sender:UIButton){
        let controller = createCustomAlertViewController()
        
        controller.presentOn(presentingViewController: self, animated: true, onDismiss: {
            
        })
    }
}
// MARK: - Network Service
extension OTPVC {
    
    private func validateFields()   ->Bool {
        
        
        let pin = self.pinView.getPin()
        
        if pin.isEmpty {
            return false
        }
        if(pin.count < 4 && pin.count > 4) {
            return false
        }
        return true
    }
    
    /***
     This method is used to Request Data for Network parameter
     
     # Usage #
     
     - To request   user  Data to verify user in our application
     
     ````
     Email , OTP
     `````
     # KEYS #
     ``````
     
     "email": "temp@temp.com"
     "otp": "0000"
     */
    
    private func getParamsForOTPverification() -> [String : Any] {
        
        
        var paramDic = [String : Any]()
        
        paramDic["email"] = self.userEmail
        paramDic["otp"] = self.pinView.getPin()
        
        return paramDic
    }
    
    
    /***
     This method is used to Request our Network
     
     # Usage #
     
     - To request verify user in our application
     
     # Network Requirements #
     
     - Base Url
     
     - Header
     
     - Parameter
     
     ````
     Email Id, otp
     
     */
    
    fileprivate func serviceRequestForOTPverification() {
        
        Service.shared.serviceRrequest(URL: URLConstants.VerifyOTP, params: getParamsForOTPverification(), header: getheaderData()) { (response) in
            print(response)
            
            self.otpResponseData = OTPResponse(fromJson: response)
            let responseCode = self.otpResponseData?.status ?? 100
            switch responseCode {
            case 200 :
                print("Succesfully registerd")
                let chooseSubscriptionVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionPlanVC") as? SubscriptionPlanVC
                self.navigationController?.pushViewController(chooseSubscriptionVC!, animated: true)
                
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.otpResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
                
                self.getAlert(self.otpResponseData?.error, actionType: .info, style: .alert)
                break
            case 400 ,401,402,404 :
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.otpResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            default:
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.otpResponseData?.error ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
                
            }
            
        }
    }
}


