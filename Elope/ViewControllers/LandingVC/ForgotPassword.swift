//
//  ForgotPassword.swift
//  Elope
//
//  Created by Anand Yadav on 27/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import TLFloatLabelTextField
import IHProgressHUD
class ForgotPassword: UIViewController {
    
    //MARK:- Instances
    @IBOutlet private weak var emailtextfield:TLFloatLabelTextField!
    
    private var forgotPasswordResponseData  : ForgotPasswordResponse?
    
    //MARK:- VIEW LIFE CYCLE METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        //Do any additional setup after loading the view.
    }
    
    //MARK:- INSTANCE METHOD
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func validate() -> Bool {
        do {
            let email = try emailtextfield.validatedText(validationType: ValidatorType.email)
            let data = RegisterData(email: email, password: "", username: "")
            print(data)
            
            return true
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
            return false
        }
    }
    
    @IBAction func onClickSubmitButton(_ sender: UIButton) {
        
        print("++++++++++++ ---------- >>>>>  Clicked On Submit ")
        
        if validate(){
            serviceRequestForForgotPassword()
        }
        
        
    }
    private func createCustomAlertViewController() -> CustomAlertMessageViewController {
        
        let controller = CustomAlertMessageViewController()
        
        controller.maxWidth = 420
        controller.minHeight = 200
        //Title
        let label = UILabel()
        label.text = "Reset Password"
        label.textAlignment = .center
        label.textColor = ColorConstants.appPrimaryColor
        label.font = UIFont(name: "Helvetica-Bold", size:17);
        controller.addArrangedSubview(view: label)
        //Image
        let imageView = UIImageView(image: UIImage (named: "Reset Password icon"))
        imageView.contentMode = .scaleAspectFit
        controller.addArrangedSubview(view: imageView, height: 120)
        //Description
        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = "A link has been sent to the email address to set a new password."
        controller.addArrangedSubview(view: descriptionLabel)
        controller.insertArrangedSubview(view: imageView, at:0)
        
        return controller
    }
    
    private func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- IBACTION METHOD
    @IBAction private func backbuttonPressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


// MARK: - Network Service
extension ForgotPassword {
    
    /***
     This method is used to Request Data for Network parameter
     
     # Usage #
     
     - To request new password for an account
     
     ````
     Email Id
     `````
     # KEYS #
     `````
     "email": "temp@temp.com",
     
     */
    
    private func getParametersForForgotPassword () -> [String:Any]{
        var paramDic = [String :Any]()
        paramDic["email"] = self.emailtextfield.text
        return paramDic
    }
    
    
    /***
     This method is used to Request our Network
     
     # Usage #
     
     - To request Reset password of an account
     
     # Network Requirements #
     
     - Base Url
     
     - Header
     
     - Parameter
     ````
     Email Id
     `````
     # KEYS #
     `````
     "email": "temp@temp.com",
     
     
     */
    fileprivate func serviceRequestForForgotPassword() {
        
        Service.shared.serviceRrequest(URL: URLConstants.ForgotPassword, params: getParametersForForgotPassword(), header: getheaderData()) { (response) in
            print(response)
            
            self.forgotPasswordResponseData = ForgotPasswordResponse(fromJson: response)
            let responseCode = self.forgotPasswordResponseData?.status ?? 100
            switch responseCode {
            case 200 :
                print("Succesfully registerd")
                let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                self.navigationController?.pushViewController(vc, animated: true)
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.forgotPasswordResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
                
                break
            case 100:
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.forgotPasswordResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            case 400 ,401,402,404 :
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.forgotPasswordResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            default:
                UIAlertController.showAlert(title: KeyConstants.APP_Name, message: self.forgotPasswordResponseData?.message ?? KeyConstants.NetworkingErrors.ServerError, viewcontroller: self,Transition: .Push)
            }
        }
    }
    
    
}

