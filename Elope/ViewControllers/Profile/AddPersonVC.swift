
//
//  AddPersonVC.swift
//  Elope
//
//  Created by veer on 11/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class AddPersonVC: UIViewController {
    
    
    let tempPerson = ["Ciara Iglesia", "Sean Mickel", "Alex kater","Jaden Smith"]
    @IBOutlet weak var personTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.InitilizeNavigation()
        // Do any additional setup after loading the view.
    }
    
    
    private func InitilizeNavigation(){
        self.navigationController?.isNavigationBarHidden = true
        personTV.register(UINib(nibName: "AddPersonCell", bundle: Bundle.main), forCellReuseIdentifier: "AddPersonCell")
        self.personTV.delegate = self
        self.personTV.dataSource = self
        
    }
    // MARK: - Navigation
    @IBAction func onClickbackButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>back button is pressed")

        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickAddPerson(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Add  person button is pressed")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPersonVC") as! SearchPersonVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
}


extension AddPersonVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempPerson.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPersonCell", for: indexPath) as! AddPersonCell
        
        if (indexPath.row == tempPerson.count) {
            cell.contentView.backgroundColor = .black
            cell.contentView.alpha = 0.5
        }
        cell.personNameLbl.text = tempPerson[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

