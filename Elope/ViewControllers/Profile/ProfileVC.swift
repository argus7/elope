//
//  ProfileVC.swift
//  Elope
//
//  Created by veer on 06/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ProfileVC: UIViewController {
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userIdLbl: UILabel!
    @IBOutlet weak var leftMilesLbl: UILabel!
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet weak var leftMileSlider: UISlider!
    
    
    
    var profileResponseData : ProfileResponse?
    /// Profile Menu array // Suggestion :  Can be Dynamic
    let profileMenuArray : [String] = {
        return ["Booked Flight", "Add People", "Flight History" ,"Direct Debit", "Top Up Mile", "Document", "Shared miles", "Edit Profile"  ]
    }()
    
    let menuImageArray : [String] = {
        return ["Booked flights icon", "Add People icon", "flight history icon" ,"Direct Debit icon", "Top Up Miles icon", "Documents icon", "Shared miles icon", "Edit Profile icon"  ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Buisness logic
        self.setNavigationBarItem()
        self.title = "Profile"
        self.initilizeView()
        
    }
    
    
    // MARK: - Initilizer (View initlized here)
    private func initilizeView (){
        profileCollectionView.register(UINib(nibName: "ProfileCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ProfileCell")
        self.leftMileSlider.setThumbImage(UIImage(named: "right plane icon pink"), for: .normal)
        self.leftMileSlider.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.6, delay: 0.2, options: .curveLinear, animations: {
            self.leftMileSlider.setValue(50, animated: true)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - All IBActions are defiend here
    
    @IBAction func onClickBackMenuTest(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickNotificationButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>  Notification button is pressed ")
    }
    
    
}

// MARK: - Profile Menu Collection View Setup Delegate and Datasource

extension ProfileVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        cell.cardView.layer.borderWidth  = 0.5
        cell.cardView.layer.borderColor = UIColor.lightGray.cgColor
        cell.menuNameLbl.text = profileMenuArray[indexPath.row]
        cell.iconImage.image = UIImage(named: menuImageArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("++++++++++++ ---------- >>>>>  Booked flight is selected")
            break
        case 1:
            print("++++++++++++ ---------- >>>>>  Add people is selected")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPersonVC") as! AddPersonVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            print("++++++++++++ ---------- >>>>>  Flight history is selected")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FlightHistoryVC") as! FlightHistoryVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 3 :
            print("++++++++++++ ---------- >>>>>  Direect debit is selected")
            break
        case 4:
            print("++++++++++++ ---------- >>>>> Top up me is selected")
            break
        case 5 :
            print("++++++++++++ ---------- >>>>>  Document is selected")
            break
        case 6:
            print("++++++++++++ ---------- >>>>>  Shared mile is selected")
            break
        case 7:
            print("++++++++++++ ---------- >>>>>  Edit profile is selected")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        default:
            print(debugDescription)
        }
    }
}

extension  ProfileVC {
    /***
     This method is used to Request Data for Network parameter
     
     # Usage #
     
     
     - To request  user profile detail
     
     ````
     userID
     `````
     # KEYS #
     ```````
     "user_id": "1",
     */
    
    private func getParamsForLogin() -> [String:Any] {
        
        var paramDic = [String : Any]()
        paramDic["email"] = self.ElopeUserID
        
        return paramDic
    }
    
    
    
    
    /***
     This method is used to Request our Network
     
     # Usage #
     
     - To request existing user to Log in our application
     
     # Network Requirements #
     
     - Base Url
     
     - Header
     
     - Parameter
     ````
     password , Email Id
     `````
     # KEYS #
     `````
     "email": "temp@temp.com",
     "password": "temp"
     
     
     */
    func serviceRequestForLogin () {
        
        Service.shared.serviceRrequest(URL: URLConstants.GetProfile, params: getParamsForLogin(), header: getheaderData()) { (response) in
            print(response)
            
            self.profileResponseData = ProfileResponse(fromJson: response)
           
            let responseCode = self.profileResponseData?.status ?? 100
            switch responseCode {
            case 200 :
                print("Succesfully registerd")
                self.getAlert(self.profileResponseData?.error, actionType: .info, style: .alert)
                break
            case 400 ,401,402,404 :
                self.getAlert(self.profileResponseData?.error, actionType: .info, style: .alert)

            default:
                self.getAlert(self.profileResponseData?.error, actionType: .info, style: .alert)

            }
        }
    }
    
    
}
