//
//  FlightHistoryVC.swift
//  Elope
//
//  Created by veer on 10/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class FlightHistoryVC: UIViewController {
    
    @IBOutlet weak var flightHistoryTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewInitilization()
        // Do any additional setup after loading the view.
    }
    
    
    private func viewInitilization () {
        self.setupRightSwipeGesture(.right)
        self.navigationController?.isNavigationBarHidden  = true
        flightHistoryTable.register(UINib(nibName: "FlightHistoryCell", bundle: Bundle.main), forCellReuseIdentifier: "FlightHistoryCell")
        flightHistoryTable.register(UINib(nibName: "FlightHistoryHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "FlightHistoryHeaderView")
        flightHistoryTable.delegate = self
        flightHistoryTable.dataSource = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden  = false

    }
    // MARK: - All IBActions
    
    @IBAction func onClickback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - flight history tableView data source and delegate implemented here !
extension FlightHistoryVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FlightHistoryHeaderView") as! FlightHistoryHeaderView
        headerView.backgroundColor = .red
        headerView.headerLbl.text = "This is Section for Header"
        headerView.headerButton.tag = section
        headerView.headerButton.addTarget(self, action: #selector(handleButtonPressed(_:)), for: UIControl.Event.touchUpInside)

        return headerView
    }
    
    @objc func handleButtonPressed(_ sender : UIButton){
        print("Hello Section \(sender.tag)")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlightHistoryCell", for: indexPath) as! FlightHistoryCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

