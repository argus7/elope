//
//  AddNewPersonVC.swift
//  Elope
//
//  Created by veer on 11/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import TLFloatLabelTextField
class SearchPersonVC: UIViewController {
    
    @IBOutlet weak var uniqueIdTextField: TLFloatLabelTextField!
    @IBOutlet weak var emailTextField: TLFloatLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.InitilizeNavigation()
        // Do any additional setup after loading the view.
    }
    
    private func InitilizeNavigation(){
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Back button is pressed")

        print("++++++++++++ ---------- >>>>> Back button is pressed")

        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Navigation
    
    @IBAction func onClickElopeButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Elope button is pressed")

    }
    
}

// MARK: - Network Method
extension SearchPersonVC {
    
    private func getparamsForAddPerson() -> [String:Any] {
        
        
        var paramDic = [String:Any]()
        
        paramDic["email"] = self.emailTextField.text
        
        return paramDic
    }
}
