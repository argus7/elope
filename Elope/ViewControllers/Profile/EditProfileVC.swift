//
//  EditProfileVC.swift
//  Elope
//
//  Created by veer on 10/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit
import TLFloatLabelTextField

class EditProfileVC: UIViewController {

    @IBOutlet weak var emailTF: TLFloatLabelTextField!
    @IBOutlet weak var userNameTF: TLFloatLabelTextField!
    @IBOutlet weak var userIdTF: TLFloatLabelTextField!
    @IBOutlet weak var leftMileslider: UISlider!
    @IBOutlet weak var userImageView: UIImageView!
    let picker = UIImagePickerController()
    
    var isFemaleSelected : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInitilization()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - View Initilization
    
    private func viewInitilization () {
        //Buisness Logic
        self.setupRightSwipeGesture(.right)
        self.navigationController?.isNavigationBarHidden = true
        self.leftMileslider.setThumbImage(UIImage(named: "right plane icon pink"), for: .normal)
        self.leftMileslider.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.6, delay: 0.2, options: .curveLinear, animations: {
            self.leftMileslider.setValue(50, animated: true)
        })
    }
    // MARK: - Navigation
    @IBAction func onClickBackButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>  Back button Is Clicked")

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDonebutton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>  Done button Is Clicked")
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickUploadPhoto(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>  upload photo button Is Clicked")
        showImageSelectionAlert(picker: picker)
  
    }
    @IBAction func onClickMaleButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Gender male is selected")
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onClickFemaleButton(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Gender Female is selected")
        sender.isSelected = !sender.isSelected
    }
    
}
