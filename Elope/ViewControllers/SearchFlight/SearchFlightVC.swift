//
//  SearchFlightVC.swift
//  Elope
//
//  Created by Anand Yadav on 02/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class SearchFlightVC: UIViewController {

    
    @IBOutlet weak var flyingFromLbl: UILabel!
    @IBOutlet weak var flyingFromNamelbl: UILabel!
    @IBOutlet weak var flyingTolbl: UILabel!
    @IBOutlet weak var flyingToDestinationNameLbl: UILabel!
    @IBOutlet weak var departureDayLbl: UILabel!
    @IBOutlet weak var departureYearDayLbl: UILabel!
    @IBOutlet weak var returnDateLbl: UILabel!
    @IBOutlet weak var returnDayYearlbl: UILabel!
    
    @IBOutlet weak var oneWaybutton: UIButton!
    @IBOutlet weak var returnButton: UIButton!
    var viewModel = SearchFlightViewModel()
    
    private var isReturnSelected :  Bool  = false {
        didSet {
            self.setupButton()
        }
    }
    //View life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        //Calling api to fetch flight list.
        let params: [String:Any] = [
            "origin": "NYC",
            "destination": "SFO",
            "departureDate": "2019-10-10"
        ]
//        viewModel.getDataFromAPIHandlerClass(objDictionary : params as! Dictionary<String, String>) { (_) in
//            
//            DispatchQueue.main.async { [weak self] in
//                //reload table
//                
//            }
//        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    private func setupButton(){
        if isReturnSelected{
            self.oneWaybutton.setBackgroundImage(nil, for: .normal)
            self.returnButton.setBackgroundImage(UIImage(named: "button"), for: .normal)
            self.returnButton.setTitleColor(.white, for: .normal)
            self.oneWaybutton.setTitleColor(.gray, for: .normal)
            
        }else{
            self.oneWaybutton.setBackgroundImage(UIImage(named: "button"), for: .normal)
            self.returnButton.setBackgroundImage(nil, for: .normal)
            self.returnButton.setTitleColor(.gray, for: .normal)
            self.oneWaybutton.setTitleColor(.white, for: .normal)
            
            
        }
        
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationBarItem()
        self.title = "Search Flight"
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear

    }
    
    
    // MARK: - All IBActions
    
    @IBAction func onClickMenu(_ sender: UIButton) {
       slideMenuController()?.toggleLeft()
    }
    
    @IBAction func onClickOneWay(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        print("++++++++++++ ---------- >>>>>  One way is selected")
        isReturnSelected = false

        //Buisness logic here
    }
    
    @IBAction func onClickReturn(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>>Return is selected")
        isReturnSelected = true

        //Buisness logic here
    }
    
    
    @IBAction func onClickSwitchLocationButton(_ sender: UIButton) {
         print("++++++++++++ ---------- >>>>>loaction switch  button is pressed")
        //Buisness logic here

    }
    @IBAction func onClickNotifationButton(_ sender: UIButton) {
         print("++++++++++++ ---------- >>>>> Notification button is Pressed ")
        //Buisness logic here

    }
    
    @IBAction func onClickDepartureDatePicker(_ sender: UIButton) {
         print("++++++++++++ ---------- >>>>> Departure datePicker button is Pressed ")
        
    }
    
    
    @IBAction func onClickReturnDatePicker(_ sender: UIButton) {
        print("++++++++++++ ---------- >>>>> Return DatePicker button is Pressed ")
        //Buisness logic here
    }
    
    @IBAction func onClickElopeBtn(_ sender: UIButton) {
        
        print("++++++++++++ ---------- >>>>> Eulope (Searched)  is selected")
        //Buisness logic here
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "SelectFlightVC") as! SelectFlightVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
