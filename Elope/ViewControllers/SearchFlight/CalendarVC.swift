//
//  CalendarVC.swift
//  Elope
//
//  Created by veer on 24/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit


class calendarCell: UICollectionViewCell{
    @IBOutlet weak var dateLbl: UILabel!
    
}
class CalendarVC: UIViewController {
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var monthYearLbl: UILabel!
    @IBOutlet weak var calendarCV: UICollectionView!
    
    
    
    let Months : [String] = {
        ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }()
    
    let DaysOfMonth = ["Sunday","Monday","Tuesday","Wednestday","Thrusday", "Friday","Saturday"]
    let DaysinMonth = [31,28,31,30,31,30,31,31,30,31,30,31]
    
    
    var numberOfEmptyBox = Int() // number of empty box in the start of current month
    var nextNumberOfEmptyBox = Int() //
    var previousNumberOfEmptyBox = Int()
    var direction = 0
    var positionIndex = 0

    
    var currentMonth = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentMonth = Months[month]
        self.monthYearLbl.text = "\(currentMonth) \(year)"
        
        calendarCV.register(UINib(nibName: "OnewayCalendarHeader", bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "OnewayCalendarHeader")
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    @IBAction func onClickPreviousMonth(_ sender: UIButton) {
        
        switch currentMonth {
        case "Jan":
            year -= 1
            month = 11

            direction = -1
            currentMonth = Months[month]
            getStartDateDayPosition()
            
            self.monthYearLbl.text = "\(currentMonth) \(year)"
            calendarCV.reloadData()
            
        default:
            month -= 1
            direction = -1
            getStartDateDayPosition()

            currentMonth = Months[month]
            self.monthYearLbl.text = "\(currentMonth) \(year)"
            
            calendarCV.reloadData()
            
        }
    }
    
    @IBAction func onClickNextMonth(_ sender: UIButton) {
        
        switch currentMonth {
        case "Dec":
            month = 0
            direction = 1

            year += 1
            getStartDateDayPosition()

            self.monthYearLbl.text = "\(currentMonth) \(year)"
            
            currentMonth = Months[month]
            calendarCV.reloadData()
        default:
            month += 1
            direction = 1
            getStartDateDayPosition()

            currentMonth = Months[month]
            self.monthYearLbl.text = "\(currentMonth) \(year)"
            
            
            calendarCV.reloadData()
            
        }
        
    }
    
    private func getStartDateDayPosition(){
        switch direction {
        case 0:
            switch day{
            case 1...7:
                numberOfEmptyBox = weekDay - day
            case 8...14:
                numberOfEmptyBox = weekDay - day - 7
            case 15...21:
                numberOfEmptyBox = weekDay - day - 14
            case 22...28:
                numberOfEmptyBox = weekDay - day - 28
            default:
                break
            }
            positionIndex = numberOfEmptyBox
            
        case 1... :
            nextNumberOfEmptyBox = (positionIndex + DaysinMonth[month])%7
            positionIndex = nextNumberOfEmptyBox
            
        case -1:
            previousNumberOfEmptyBox = (7 - (DaysinMonth[month] - positionIndex)%7)
            if previousNumberOfEmptyBox == 7 {
                previousNumberOfEmptyBox = 0
            }
            positionIndex = previousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
}
    
    
    extension CalendarVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            switch direction {
            case 0:
                return DaysinMonth[month] + numberOfEmptyBox
            case 1...:
                return DaysinMonth[month] + nextNumberOfEmptyBox
            case -1:
                return DaysinMonth[month] + previousNumberOfEmptyBox
            default:
                fatalError()
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calendarCell", for: indexPath) as! calendarCell
            
            switch direction {
            case 0:
                 cell.dateLbl.text = "\(indexPath.row + 1 - numberOfEmptyBox)"
            case 1...:
                cell.dateLbl.text = "\(indexPath.row + 1 - nextNumberOfEmptyBox)"

            case -1:
                cell.dateLbl.text = "\(indexPath.row + 1 - previousNumberOfEmptyBox)"

            default:
                fatalError()
            }
            
            if Int(cell.dateLbl.text!)! < 1{
                cell.isHidden = true
            }
           
            return cell
        }
        
        
        
        
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            
            
            switch kind {
                
            case UICollectionView.elementKindSectionHeader:
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "OnewayCalendarHeader", for: indexPath) as! OnewayCalendarHeader
                return headerView
            default:
                return UICollectionReusableView()
            }
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width: collectionView.frame.width, height: 50)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: (collectionView.frame.width / 7), height: (collectionView.frame.width / 7))
        }
}

