//
//  SelectFlightVC.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit


class SelectFlightVC: UIViewController {
    
    
    @IBOutlet weak var flyingFromAirportCodeLbl: UILabel!
    
    @IBOutlet weak var flyingFromPlaceNameLbl: UILabel!
    
    @IBOutlet weak var flyingToAirportCodeLbl: UILabel!
    
    public var searchedType = SearchedFlightType.OneWay
    
    
    @IBOutlet weak var flyingToPlaceNamelbl: UILabel!
    
    @IBOutlet weak var searchedFlightTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        print("++++++++++++ ---------- >>>>>  here Flight selection list will be shown")
        // buisness logic
        self.initilizeTableCell()
        self.viewInitilization()

    }
    
    override func viewDidLayoutSubviews() {
        self.viewInitilization()

    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func viewInitilization(){
        self.navigationController?.isNavigationBarHidden = true
        self.setupRightSwipeGesture(.right)

    }
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        
        print("++++++++++++ ---------- >>>>>  Back button is pressed ")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNotifiactionButton(_ sender: UIButton) {
         print("++++++++++++ ---------- >>>>>  Notification button is pressed ")
    }
    
}
//MARK:- seacrhed Flight Listing , Table View  Delegate and datasource

extension SelectFlightVC : UITableViewDelegate , UITableViewDataSource {
    
    private func initilizeTableCell() {
        switch searchedType {
        case .OneWay:
            searchedFlightTable.register(UINib(nibName: "ReturnFlightCell", bundle: Bundle.main), forCellReuseIdentifier: "ReturnFlightCell")

//            searchedFlightTable.register(UINib(nibName: "OnewayFlightCell", bundle: Bundle.main), forCellReuseIdentifier: "OnewayFlightCell")
        case .Return:
            searchedFlightTable.register(UINib(nibName: "ReturnFlightCell", bundle: Bundle.main), forCellReuseIdentifier: "ReturnFlightCell")
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch searchedType {
        case .OneWay:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnFlightCell", for: indexPath) as! ReturnFlightCell

//            let cell = tableView.dequeueReusableCell(withIdentifier: "OnewayFlightCell", for: indexPath) as! OnewayFlightCell
            print("++++++++++++ ---------- >>>>>  one way flight result is showing ")
          
            return cell
            
        case .Return:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnFlightCell", for: indexPath) as! ReturnFlightCell
            print("++++++++++++ ---------- >>>>>  Both way flight result is showing ")
            
            return cell
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
}

