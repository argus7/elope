//
//  LeftMenuVC.swift
//  Elope
//
//  Created by Anand Yadav on 02/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case searchFlight = 0
    case profile
    
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftMenuProtocol {

    var cellitem:[String] = []
    var searchFlightViewController: UIViewController!
    var profileViewController: UIViewController!
    @IBOutlet private var tablemenu:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        cellitem = ["Home","Profile","Help","Settings","Upgrade Subscription","Miles top up","Share miles","Logout"]
        self.tablemenu.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        //Set up left Menu Panel Controllers
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.searchFlightViewController = storyboard.instantiateViewController(withIdentifier: "SearchFlightVC") as! SearchFlightVC
        self.searchFlightViewController = UINavigationController(rootViewController: searchFlightViewController)

        
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.profileViewController = UINavigationController(rootViewController: profileViewController)

}
    
    //MARK:- LEFTMENU PROTOCOL METHOD
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .searchFlight:
            self.slideMenuController()?.changeMainViewController(self.searchFlightViewController, close: true)
        case .profile:
            self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
        }
    }
    
    //MARK: TABLE DELEGATE AND DATASOURCE METHOD
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellitem.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if( !(cell != nil))
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        }
        
        cell!.textLabel?.text = cellitem[indexPath.row]
        if indexPath.row == 7 {
            cell!.textLabel?.textColor = ColorConstants.appPrimaryColor
            cell!.textLabel?.font = UIFont(name: "Helvetica-Bold", size:16);
        }
        
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
}
