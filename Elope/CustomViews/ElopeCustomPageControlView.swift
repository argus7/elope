//
//  ElopeCustomPageControlView.swift
//  Elope
//
//  Created by veer on 09/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import UIKit


class ElopeCustomPageControlView : UIPageControl {
    
    var currentPageImage :  UIImage?
    var tintImage : UIImage?
    var cPageIndicatorColor : UIColor = UIColor.appColor
    var pageIndicatorTColor : UIColor = UIColor.lightGray
    var isIndicatorChanged : IndicatorType = IndicatorType.color
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        switch isIndicatorChanged {
        case .Image:
            self.pageIndicatorTintColor = UIColor.init(patternImage: tintImage!)
            self.currentPageIndicatorTintColor = UIColor.init(patternImage: currentPageImage!)
        case .color:
            self.pageIndicatorTintColor = pageIndicatorTColor
            self.currentPageIndicatorTintColor = cPageIndicatorColor
        }
    }
    
}


