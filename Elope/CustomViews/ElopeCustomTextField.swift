//
//  ElopeCustomTextField.swift
//  Elope
//
//  Created by veer on 10/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
class ElopeCustomTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        drawBottomBarTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    
    private func drawBottomBarTextField() {
        let bottomBarView = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.height + 5, width: self.frame.width, height: 1))
        bottomBarView.backgroundColor = UIColor.lightGray
        
//        bottomBarView.clipsToBounds = true
       
        self.addSubview(bottomBarView)
        self.bringSubviewToFront(bottomBarView)
//         self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        drawBottomBarTextField()
    }
}
