//
//  SelectFlightViewClass.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation

public class ClipedCircleView: UIView {
    
    private let leftCircle = UIView(frame: .zero)
    private let rightCircle = UIView(frame: .zero)
    
    public var circleY: CGFloat = 0
    public var circleRadius: CGFloat = 10
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        addSubview(leftCircle)
        addSubview(rightCircle)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        leftCircle.backgroundColor = superview?.backgroundColor
        leftCircle.frame = CGRect(x: -circleRadius, y: circleY,
                                  width: circleRadius * 2 , height: circleRadius * 2)
        leftCircle.layer.masksToBounds = true
        leftCircle.layer.cornerRadius = circleRadius
        
        rightCircle.backgroundColor = superview?.backgroundColor
        rightCircle.frame = CGRect(x: bounds.width - circleRadius, y: circleY,
                                   width: circleRadius * 2 , height: circleRadius * 2)
        rightCircle.layer.masksToBounds = true
        rightCircle.layer.cornerRadius = circleRadius
    }
}
