
//
//  SearchFlightClassView.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation


class circleView : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.height/2
    }
    
    
}

class GradientButton : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = 5.0
    }
    
}

class SegmentView : UISegmentedControl {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
     
    }
    
}

