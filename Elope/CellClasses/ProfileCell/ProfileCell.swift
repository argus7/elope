//
//  ProfileCell.swift
//  Elope
//
//  Created by veer on 06/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class ProfileCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var menuNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
    }
//    override var isSelected: Bool{
//        didSet{
//            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
//                self.transform = self.isSelected ? CGAffineTransform(scaleX: 1.1, y: 1.1) : CGAffineTransform.identity
//            }, completion: nil)
//
//        }
//    }
}
