//
//  OnewayFlightCell.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class OnewayFlightCell: UITableViewCell {

    @IBOutlet weak var cardView: ClipedCircleView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.circleY = cardView.frame.height/2
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
