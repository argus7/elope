//
//  ReturnFlightCell.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class ReturnFlightCell: UITableViewCell {

    @IBOutlet weak var cardView: ClipedCircleView!
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.circleRadius = 10
        cardView.circleY = self.frame.height/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
