//
//  FlightHistoryCell.swift
//  Elope
//
//  Created by veer on 10/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class FlightHistoryCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var destinationAirportNameLbl: UILabel!
    @IBOutlet weak var fromAirportNamelbl: UILabel!
    @IBOutlet weak var destinationCityName: UILabel!
    @IBOutlet weak var fromCityName: UILabel!
    @IBOutlet weak var destinationCityCodeLbl: UILabel!
    @IBOutlet weak var fromCityCodeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
