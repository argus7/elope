//
//  AddPersonCell.swift
//  Elope
//
//  Created by veer on 11/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class AddPersonCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var personIV: UIImageView!
    @IBOutlet weak var personNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
