//
//  LoginData.swift
//  Elope
//
//  Created by veer on 18/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import SwiftyJSON
struct LoginResponse {
    
    var error : String!
    var status : Int!
    var success : Bool!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        error = json["error"].stringValue
        status = json["status"].intValue
        success = json["success"].boolValue
    }
}

