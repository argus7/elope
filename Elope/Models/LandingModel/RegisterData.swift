//
//  RegisterData.swift
//  Elope
//
//  Created by Anand Yadav on 03/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import SwiftyJSON


class RegistrationResponse : NSObject{
    
    var message : String!
    var status : Int!
    var success : Bool!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        status = json["status"].intValue
        success = json["success"].boolValue
    }
}



struct RegisterData {
    var email: String
    var password: String
    var username: String
}
