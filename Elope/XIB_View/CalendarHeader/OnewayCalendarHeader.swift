//
//  OnewayCalendarHeader.swift
//  Elope
//
//  Created by veer on 24/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class OnewayCalendarHeader: UICollectionReusableView {
    @IBOutlet weak var monthLbl: UILabel!
    
    @IBOutlet weak var yearLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
