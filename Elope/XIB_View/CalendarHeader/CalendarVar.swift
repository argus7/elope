//
//  CalendarVar.swift
//  Elope
//
//  Created by veer on 24/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation



let date = Date()
let calendar = Calendar.current


let day = calendar.component(.day, from: date)
let weekDay = calendar.component(.weekday, from: date)
var month = calendar.component(.month, from: date)
var year = calendar.component(.year, from: date)

