//
//  FlightHistoryHeaderView.swift
//  Elope
//
//  Created by veer on 10/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import UIKit

class FlightHistoryHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    
}
