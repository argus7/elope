//
//  URLConstants.swift
//  Elope
//
//  Created by Anand Yadav on 23/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation

struct URLConstants {
    
    private struct Domains {
        static let Dev = ""
        static let UAT = ""
        static let Local = ""
        static let QA = ""
        static let Staging = "http://elopapi.stagingenv.co.in"
    }
    
    private  struct Routes {
        static let Api = "/user/"
    }
    
    private  static let Domain = Domains.Staging
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route
    
    static var SocialLogin: String {
        return BaseURL  + ""
    }
    
    static var SignUp: String {
        return BaseURL  + "register"
    }
    
    static var Login: String {
        return BaseURL  + "login"
    }
    
    static var ForgotPassword: String {
        return BaseURL  + "forgot-password"
    }
    
    static var ChangePassword: String {
        return BaseURL  + ""
    }
    
    static var VerifyOTP: String {
        return BaseURL  + "verifyotp"
    }
    
    static var ResetPassword: String {
        return BaseURL  + "resetpassword"
    }
    
    static var Logout: String {
        return BaseURL  + "logout"
    }
    
    static var EditProfile: String {
        return BaseURL  + "editprofile"
    }
    
    static var GetProfile: String {
        return BaseURL  + "getprofile"
    }
    
    static var SearchFlights: String {
        return BaseURL  + "flights"
    }
}
