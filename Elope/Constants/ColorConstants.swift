//
//  ColorConstants.swift
//  Elope
//
//  Created by Anand Yadav on 23/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation


//ColorConstants.swift
struct ColorConstants {
    
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
    
    static let appPrimaryColor =   UIColor(red: 184.0/255.0, green: 4.0/255.0, blue: 84.0/255.0, alpha: Alphas.Opaque)
    static let appSecondaryColor =   UIColor(red: 184.0/255.0, green: 4.0/255.0, blue: 84.0/255.0, alpha: Alphas.Opaque)
    
    struct TextColors {
        static let Error = ColorConstants.appSecondaryColor
        static let Success =  UIColor(red: 184.0/255.0, green: 4.0/255.0, blue: 84.0/255.0, alpha: Alphas.Opaque)
    }
    
    struct OverlayColor {
        static let SemiTransparentBlack = UIColor.black.withAlphaComponent(Alphas.Transparent)
        static let SemiOpaque = UIColor.black.withAlphaComponent(Alphas.SemiOpaque)
        static let demoOverlay = UIColor.black.withAlphaComponent(0.6)
    }
    
}
