//
//  KeyConstants.swift
//  Elope
//
//  Created by Anand Yadav on 24/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation


//KeyConstants.swift
struct KeyConstants {
    
    static let DeviceType = "iOS"
    static let APP_Name = "Elope"
    
    struct UserDefaults {
        static let k_App_Running_FirstTime   = "userRunningAppFirstTime"
        
        static let kIsUseregistered          =  "isUseregistered"
        static let kDeviceToken              =  "deviceToken"
        static let kDeviceID                 =  "deviceID"
        static let kUserName                 =  "userName"
        static let kUserMobileNumber         =  "userMobileNumber"
        static let kUserEmail                =  "userEmail"
        static let kUserPassword             =  "userPassword"
        static let kUserDob                  =  "userDob"
        static let kUserId                   =  "userID"
        static let kUserLoginType            =  "loginType"
        static let kUserImage                =  "userImage"
        static let kUserRemembered           =  "isRemembered"
        static let kUserGender               =  "userGender"
        static let kUserSecuirityToken       =  "secuirityToken"
    }
    
    struct Headers {
        static let Authorization = "sdb38hjie48947ndsk7845wjkh"
        static let ContentType = "application/json"
    }
    
    struct Google{
        static let googleKey = "some key here"
        
    }
    
    struct Facebook{
        static let facebookKey = "some key here"
        
    }
    
    struct AlertConstant {
        static let logoutAlertMessage =   "Are you sure, you want to logout !!"
        
    }
    
    struct NetworkingErrors {
        static let ConnectionError = "Please check your internet connection"
        static let ServerError = "Server error, Please try again in a moment"
    }
    
}


