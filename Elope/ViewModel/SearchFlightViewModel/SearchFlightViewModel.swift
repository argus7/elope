//
//  SearchFlightViewModel.swift
//  Elope
//
//  Created by Anand Yadav on 23/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import Alamofire

class SearchFlightViewModel{
    
    typealias completionBlock = ([SearchFlightModel]) -> ()
    var datasourceArray = [SearchFlightModel]()
    
    let headers: HTTPHeaders = [
        "x-secret-token": (KeyConstants.Headers.Authorization).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!,
        "Content-Type" : (KeyConstants.Headers.ContentType ).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!,
    ]
    
    func getDataFromAPIHandlerClass(objDictionary:Dictionary<String,String>, completionBlock: @escaping completionBlock){
      
        Service.shared.serviceRrequest(URL: URLConstants.SearchFlights, params: objDictionary, header: headers) { (response) in
            print(response)
            
        }
    }
    
    func getNumberOfRowsInSection() -> Int{
        
        return datasourceArray.count
    }
    
    func getUserAtIndex(index : Int) -> SearchFlightModel{
        
        let flightresult = datasourceArray[index]
        return flightresult
    }
    
    func getCellData(index : Int) -> String{
        
        return ""
    }
     
}
