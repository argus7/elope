//
//  UIAlertController+Extention.swift
//  Elope
//
//  Created by veer on 20/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import UIKit

enum TransitionMove {
    case Push
    case Pop
}

extension UIAlertController {
    
    /***
     
     This method is used to Get Alert Message with type
     
     # Parameters #
     - Title: App Name
     - Message: Meassge for the event
     - ActionType:  cameraEvent , InfoEvent , ActionEvent
     
     # USAGE #
     ````
     title : string , message : string , ActionType :  AlertBoxType
     ````
     */
    
    

     convenience init(title : String , message : String,ActionType : AlertBoxEvent ,styleType : UIAlertController.Style) {
        
        self.init()
        self.title = title
        self.message = message
        
        switch ActionType {
        case .action:
            let alertActionOk =  UIAlertAction.init(title: "Ok", style: .default)
            let alertActionCancel  =  UIAlertAction.init(title: "Cancel", style: .default)
            self.addAction(alertActionOk)
            self.addAction(alertActionCancel)
            break
        case .camera:
            let alertActionPhoto =  UIAlertAction.init(title: "Photo", style: .default)
            let alertActionCamera  =  UIAlertAction.init(title: "Camera", style: .default)
            self.addAction(alertActionPhoto)
            self.addAction(alertActionCamera)
            break
        case .info:
            let alertActionOk =  UIAlertAction.init(title: "Ok", style: .default)
            self.addAction(alertActionOk)
            break
        }
    }
    
    /// pop to previous view with a message
    ///
    /// - Parameter message: String ("Done")
    
    convenience init (title : String , message : String ) {
        
        self.init()
        self.title = title
        self.message = message
        let backAction = UIAlertAction(title: "Ok", style: .destructive) { (action : UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
        self.addAction(backAction)
    }
    
    
    static func showAlert(title: String, message:String, viewcontroller:UIViewController , Transition : TransitionMove){
        switch Transition {
        case .Push:
            let alert = UIAlertController(title: title, message: message, ActionType: .info, styleType: .alert)
            viewcontroller.present(alert, animated: true)
        case .Pop:
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            viewcontroller.present(alert, animated: true)
        }
    }
}
