//
//  ElopeExtention.swift
//  Elope
//
//  Created by Anand Yadav on 27/08/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
//    var statusBarView: UIView? {
//        if responds(to: Selector(("statusBar"))) {
//            return value(forKey: "statusBar") as? UIView
//        }
//        return nil
//    }
}

extension UIButton {
    func halfTextColorChange(fullText : String , changeText : String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: ColorConstants.appPrimaryColor , range: range)
        self.setAttributedTitle(attribute, for: .normal)
    }
}

extension UITextField {
    
    
    /**
     This function is used to validate Textfield on the base of their input
     ### Usage ###
     
     - validate the input filled
     
     # Input Type #
     
     - Enum is defined for the input i.e : Validator
     - validator :  email, password, username
     
     # Example #
     ```
     
     let personName = VaildatorFactory.validatorFor(type: validationType.name)
     
     */
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}


extension UIColor {
   
    open class var gradientLeftColor: UIColor { get {
        return UIColor(red: 184.0/255.0, green: 4.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        }
    }
    open class var gradientRightColor: UIColor { get {
        return UIColor(red: 184.0/255.0, green: 4.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        }
    }
    
    open class var appColor: UIColor { get {
            return UIColor(red: 200.0/255.0, green: 28.0/255.0, blue: 95.0/255.0, alpha: 1.0)
        }
    }
}

extension CALayer {
    
    func addBorder(edges: [UIRectEdge], color: UIColor, thickness: CGFloat) {
        
        for edge in edges {
            let border = CALayer()
            switch edge {
            case .top:
                border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
                border.backgroundColor = color.cgColor;
                addSublayer(border)
            case .bottom:
                border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
                border.backgroundColor = color.cgColor;
                addSublayer(border)
            case .left:
                border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
                border.backgroundColor = color.cgColor;
                addSublayer(border)
            case .right:
                border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
                border.backgroundColor = color.cgColor;
                addSublayer(border)
            default:
                break
            }
        }
       
    }
    
    
}

