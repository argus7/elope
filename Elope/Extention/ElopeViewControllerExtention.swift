//
//  ElopeViewControllerExtention.swift
//  Elope
//
//  Created by veer on 13/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import Alamofire
extension UIViewController {
    
    /// Get Main StoryBoard
    
    var ElopeStoryBoard : UIStoryboard {
        get{
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    /// Get screen width ratio
    open var widthRatio : CGFloat {
        get {
            return UIScreen.main.bounds.width / 320
        }
    }
    
    /// Get current screen Height ratio
    open var heightRatio : CGFloat {
        get {
            return UIScreen.main.bounds.height / 568
        }
    }
    
    /// Get current user id
    var ElopeUserID : String {
        get {
            return UserDefaults.standard.value(forKey:KeyConstants.UserDefaults.kUserId) as! String
        }
    }
    
    /// Set navigation bar
    func setNavigationBarItem() {
        let image : UIImage? = UIImage(named:"menu icon")!.withRenderingMode(.alwaysOriginal)
        
        self.addLeftBarButtonWithImage(image!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        self.navigationController?.navigationBar.updateConstraintsIfNeeded()
    }
    
    /// Remove navigation bar
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    func setNormalNavigationBarItem(_ navigationTitle : String) {
        let image : UIImage? = UIImage(named:"menu icon")!.withRenderingMode(.alwaysOriginal)
        self.navigationController?.navigationBar.backIndicatorImage = image
        self.navigationController?.title = navigationTitle
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    /// Get header Data for Authorization
    ///
    /// - Returns: HTTP header Type
    func getheaderData() -> HTTPHeaders {
        
        //    x-secret-token:sdb38hjie48947ndsk7845wjkh
        //    Content-Type:application/json
        
//        let udid = UserDefaults.standard.value(forKey: userDefaultConstants.kDeviceID) ?? "qwertyisakey"
//        let securityToken = UserDefaults.standard.value(forKey:userDefaultConstants.kUserSecuirityToken) ?? "1234567890"
        let headers: HTTPHeaders = [
            "x-secret-token": (KeyConstants.Headers.Authorization).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!,
            "Content-Type" : (KeyConstants.Headers.ContentType ).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!,
        ]
        return headers
    }
    
    /// Set navigation Bar status (hidden or visible)
    ///
    /// - Parameter Value: True or False (Default is False)
    func setNavigationBarHidden(Value : Bool){
        self.navigationController?.setToolbarHidden(Value, animated: false)
    }
    
    /// Set Navigation Bar With Custom Color and and Custom Image
    ///
    /// - Parameters:
    ///   - imageName: Name of the image
    ///   - navigationBarColor: color for navigation Bar
    
    func setNavigationBar(imageName : UIImage, navigationBarColor : UIColor){
        self.navigationController?.navigationBar.backIndicatorImage = imageName
        self.navigationController?.navigationBar.backgroundColor =  navigationBarColor
    }
}




extension UIViewController {
    
    
    /// Aler message for image Picker
    ///
    /// - Parameter picker: picker view
    func showImageSelectionAlert(picker : UIImagePickerController){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Camera")
            self.getImageByCamera(picker: picker)
        }
        let galleryAction = UIAlertAction(title: "Album", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Album")
            self.getImageByPhotoStorage(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("No")
        }
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIViewController {
    //MARK: - ALERT Controller and ImagePickerController Delegate
    func getImageByCamera(picker : UIImagePickerController){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        }
        else{
            self.getAlert("No camera", actionType: .info, style: .alert)
        }
    }
    func getImageByPhotoStorage(picker : UIImagePickerController){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    
    
    /// Date validation with two dates
    ///
    /// - Parameters:
    ///   - pickedDate: selected date
    ///   - comparedDate: refrence date
    ///   - caparedByType: datecompared by (previous, current, futureq)
    /// - Returns: Bool vallue
    func validateDateAccordingly (pickedDate : Date, comparedDate: Date, caparedByType : dateComparedBy ) -> Bool{
        switch  caparedByType {
        case .equal:
            if pickedDate == comparedDate{
                return true
            }
            return false
        case .greaterThan :
            if pickedDate > comparedDate{
                return true
            }
            return false
        case .lessThan :
            if pickedDate < comparedDate{
                return true
            }
            return false
            
        }
    }
}


extension UIViewController {
    func getAlert(_ alertMessage : String? , actionType : AlertBoxEvent , style :UIAlertController.Style){
        let alertController = UIAlertController(title: KeyConstants.APP_Name, message: alertMessage ?? KeyConstants.NetworkingErrors.ConnectionError, ActionType: actionType, styleType: style)
            self.present(alertController, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    /// get updated frame of viewController
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update frame
    func getUpdatedFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.height *= heightRatio
        tempFrame.size.width *= widthRatio
        return tempFrame
    }
    
    /// get updated width frame of View
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update  width of the frame
    func getUpdatedWidthFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.width *= widthRatio
        return tempFrame
    }
    
    
    /// get updated width frame of View
    ///
    /// - Parameter frame: pass your frame
    /// - Returns: update  Height of the frame
    func getUpdatedHeightFrame(frame : CGRect) -> CGRect {
        var tempFrame = frame
        tempFrame.size.height *= heightRatio
        return tempFrame
    }
    
    /// Logout user from application
    func logoutUser(){
        let alerBox = UIAlertController(title:KeyConstants.APP_Name, message: "Are you sure, you want to logout !!", preferredStyle: .alert)
        let logoutAction = UIAlertAction(title: "Logout", style: .destructive) { (action : UIAlertAction) in
            
            UserDefaults.standard.set(false, forKey: KeyConstants.UserDefaults.kUserRemembered)
            let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action : UIAlertAction) in
        }
        alerBox.addAction(cancel)
        alerBox.addAction(logoutAction)
        
        self.present(alerBox, animated: true)
    }
    
    
    /// If user login from anothe device
    
    func showAuthError(){
        let alerBox = UIAlertController(title: KeyConstants.APP_Name, message: "Looks you have already logged in from another device, you need to login again", preferredStyle: .alert)
        let logoutAction = UIAlertAction(title: "Ok", style: .destructive) { (action : UIAlertAction) in
            
            UserDefaults.standard.set(false, forKey: KeyConstants.UserDefaults.kUserRemembered)
            let vc = self.ElopeStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alerBox.addAction(logoutAction)
        self.present(alerBox, animated: true)
    }
    
    
    
    func setupRightSwipeGesture(_ gestureDirection : UISwipeGestureRecognizer.Direction?) {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = gestureDirection ?? .right
        
        swipeRight.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            print("****************  S w i p e R i g h t ****************")
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            print("*************** S w i p e L e f t **************")
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.up {
            print("***************** S w  i p e U p *****************")
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            print("**************** S w i p e D o w n *************** ")
        }
    }
    
    /// Pop to previous Page
    func gotoPreviousPage()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /// Go to specific view controlle which is in Navigation Stack
    ///
    /// - Parameter yourController: specific viewController name
    func popToSpecficViewController(yourController : UIViewController){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        if viewControllers.count > 0{
            for aViewController in viewControllers {
                if(aViewController  == yourController  ){
                    self.navigationController!.popToViewController(aViewController, animated: false);
                    return
                }
            }
        }
        
    }
    
    /// Get Gender in string
    ///
    /// - Parameter genderStr: It can be string or Integer ,
    /// - Returns: String (male or Female)
    func getGender<T>(genderStr : T) -> String {
        
        let inputData = "\(genderStr)"
        if inputData == "1" {
            return "Male"
        }
        return "Female"
        
    }
    
    public func convertDataIntoJSON <T : Encodable>(requestedData : T) -> Data {
        
        let encoder = JSONEncoder()
        var result = Data()
        do {
             result = try encoder.encode(requestedData)
        }catch{
            print("Data can't be convert in to JSON ")
        }
        return result
    }
    
}



