//
//  UIStoryBoard+Extention.swift
//  Elope
//
//  Created by veer on 20/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation

protocol StringConvertible {
    var rawValue: String {get}
}

protocol Instantiable: class {
    static var storyboardName: StringConvertible {get}
}

extension Instantiable {
    static func instantiateFromStoryboard() -> Self {
        return instantiateFromStoryboardHelper()
    }
    
    private static func instantiateFromStoryboardHelper<T>() -> T {
        let identifier = String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}

//MARK: -

extension String: StringConvertible { // allow string as storyboard name
    var rawValue: String {
        return self
    }
}
extension UIStoryboard {
   
    static func pushToController(_ controller : UIViewController){
    }
   
    
}
