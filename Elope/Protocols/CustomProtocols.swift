//
//  CustomProtocols.swift
//  Elope
//
//  Created by veer on 05/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation

protocol ListTypeProtocol {
    
    func getListType(_ input : Int) -> SearchedFlightType
    
}

protocol GetAlertMessage {
    
}
extension GetAlertMessage {
    static func getAlertMessage(Title : String , Message : String,ActionType : AlertBoxEvent ,styleType : UIAlertController.Style ){
        
    }
}
