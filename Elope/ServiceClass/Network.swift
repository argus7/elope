//
//  Network.swift
//  Elope
//
//  Created by veer on 13/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire
import IHProgressHUD
import SwiftyJSON

class Service {
    private init(){}
    
    static let shared = Service()

    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    
    func serviceRrequest(URL : String , params : [String:Any],header : HTTPHeaders? ,completionHandler :  @escaping (_ result : JSON) -> ()){
        
        
        IHProgressHUD.show()
        Alamofire.request(URL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
            IHProgressHUD.dismiss()
            guard responseData.result.isSuccess else {
                print("Error iN API")
                return
            }
            
             let jsonData = JSON(responseData.result.value!)
            completionHandler(jsonData)
        }
    }
    
}


