//
//  ElopeEnums.swift
//  Elope
//
//  Created by veer on 12/09/19.
//  Copyright © 2019 Nexcode. All rights reserved.
//

import Foundation


enum StoryboardName: String, StringConvertible {
    case main = "Main"
}

enum LoginType : Int{
    case Email = 0
    case Social
}


/// Validation
///
/// - email: textField type Email
/// - password: textField type password
/// - username: textField type username
/// - requiredField: textField type requiredField

enum ValidatorType {
    case email
    case password
    case username
    case requiredField(field: String)
}

/// Segment control Indicatior type
///
/// - color: UIColor type
/// - Image: image
enum IndicatorType : Int {
    case color = 1
    case Image
}

/// which type of Flight Search
///
/// - OneWay: OneWay journey
/// - Return: Both way journey
enum SearchedFlightType : Int {
    case OneWay = 1
    case Return
}

/// this Enum property is created for Alert box Type
///
/// - cameraEvent: to upload any image
/// - InfoEvent: to show message

/// - ActionEvent: to perform any action with message
enum  AlertBoxEvent : Double {
    case camera = 0
    case info
    case action
}

/// Compare date with their valuew
///
/// - lessThan:  Given date is past
/// - greaterThan: Given  Date is in Future
/// - equal:  given date is current date
enum dateComparedBy : Int {
    case lessThan = 1
    case greaterThan
    case equal
}

/// Calendar type
///
/// - One way = Return days View
/// - Round :  Return Departure date , return date
enum CalendarType : Int {
    case OneWay = 1
    case Round
}

